# Cool additions to the standard Linux 18.04 with Gnome

sudo apt-get update

sudo apt-get upgrade

sudo apt install git-all

### Software: #
        * Chrome
        * Spotify
        * VS Code
        * GitKraken
        * Android Studio
        * VLC

sudo apt install gnome-tweak-tool

sudo apt install gnome-shell-extensions

gedit .bashrc

### Extensions: #
        * User Themes
        * Extended Gestures
        * OpenWeather
        * Netspeed
        * Workspaces to dock
        * Auto move windows
        * Cover flow alt-tab
        * Dash to Dock
        * Todo.txt
        * Clipboard indicator
        * Dinamyc top bar
        * CPU Power Manager
        * No title bar
        * Panel OSD
        * Refresh Wifi connections
        * Apt update indicator

### Themes

> GTK3: Arc Theme

sudo apt install arc-theme

> Icons: La Capitanea

cd .icons/
git clone https://github.com/keeferrourke/la-capitaine-icon-theme.git

> [Eliminar background do switcher] (https://github.com/passingthru67/workspaces-to-dock/issues/75)

Apagar os dash.svg da dir /Arc

### Gestures

sudo gpasswd -a $USER input
sudo apt-get install libinput-tools
sudo apt-get install xdotool  
sudo apt install ruby  
sudo gem install fusuma  
cd ~/.config    
mkdir fusuma  
cd fusuma/
touch config.yml
gedit config.yml
sudo fusuma  
which fusuma = (/usr/local/bin/fusuma -d)
gnome-session-properties

> Adicionar o path do fusuma às starting apps (adicionar -d no fim do comando)

### How to Design Changes #
> https://www.youtube.com/watch?v=sfsKwzElxQg